var mongoose = require('mongoose');

mongoose.Promise = require('bluebird');

var mongodbUrl = process.env.MONGODB_URI;

mongoose.connect(mongodbUrl, {
  useMongoClient: true
});

module.exports = mongoose;