var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var AccidentSchema  = require('./schema')(Schema);

AccidentSchema.pre('save', function(next) {
    if(!this.createdAt)
        this.createdAt = new Date();
    this.updatedAt = Date();
    next();
});

var Accident = mongoose.model('Accident', AccidentSchema);

exports.accidentModel = Accident;
exports.accidentSchema = AccidentSchema;