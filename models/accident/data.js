module.exports = {
    createdAt: { type: Date, default: new Date() },
    updatedAt: { type: Date, default: new Date() },
    title: { type: String, required: true }
};

