var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var PersonSchema  = require('./schema')(Schema);

PersonSchema.pre('save', function(next) {
    if(!this.createdAt)
        this.createdAt = new Date();
    this.updatedAt = Date();
    next();
});

var Person = mongoose.model('Person', PersonSchema);

exports.personModel = Person;
exports.personSchema = PersonSchema;