module.exports = {
    createdAt: {type: Date, default: new Date()},
    updatedAt: {type: Date, default: new Date()},
    role: {type: String, required: true},
    state: {type: String, required: true},
    age: {type: Number, required: true},
    _accident: {type: String, ref: 'Accident', default: null}
};