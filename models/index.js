var Accident = require('./accident');
var Person = require('./person');
var Post = require('./post');

exports.Accident = Accident.accidentModel;
exports.Person = Person.personModel;
exports.Post = Post.postModel;