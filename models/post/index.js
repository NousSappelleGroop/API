var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var PostSchema  = require('./schema')(Schema);

PostSchema.pre('save', function(next) {
    if(!this.createdAt)
        this.createdAt = new Date();
    this.updatedAt = Date();
    next();
});

var Post = mongoose.model('Post', PostSchema);

exports.postModel = Post;
exports.postSchema = PostSchema;