module.exports = {
    createdAt: {type: Date, default: new Date()},
    updatedAt: {type: Date, default: new Date()},
    title: {type: String, required: true},
    author: {type: String, required: true},
    _accident: {type: String, ref: 'Accident', default: null}
};
