## To do

Clone the repo: `git clone --recursive git@github.com:NousSappelleGroop/API.git`

Create a `.env` file in the root directory of the project. Add
variables:

```
PORT=8080
MONGODB_URI=mongodb://localhost:2000/nuitinfo
SESSION_NAME=my_session_name
SESSION_SECRET=my_session_secret
```

Install dependencies `npm install`

Start mongodb server: `mongod --port=2000`

Run `npm start` to start the server

You can now visit the website on `localhost:8080`.

