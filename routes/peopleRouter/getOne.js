var models = require('../../models');

module.exports = function getOne (req, res) {

    var q = models.Person.findById(req.params.person_id);

    q.exec(function(err, person){
        if (err) { console.log(err); return res.sendStatus(500); }
        if (!person) { return res.status(404).json({error: 'cannot find collection with id: '+req.params.person_id}) }

        res.json({data: person});
    })

}