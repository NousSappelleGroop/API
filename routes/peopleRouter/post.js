var models = require('../../models');
var personData = require('../../models/person/data');

module.exports = function post(req, res) {

    var person = new models.Person();

    for (var prop in personData) {
        if (personData.hasOwnProperty(prop)) {
            if (req.body[prop]) {
                person[prop] = req.body[prop];
            }
            else if (personData[prop].default !== undefined) {
                person[prop] = personData[prop].default;
            }
            else {
                res.status(400).send({error: "some required parameters were not provided or don't respect the required format"});
                res.end();
                return;
            }
        }
    }

    person.save(function (err) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
            return;
        }
        res.json(person);
    });
}