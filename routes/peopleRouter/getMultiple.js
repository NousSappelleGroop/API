var models = require('../../models');

module.exports = function getMultiple(req, res) {

    var q = models.Person.find().sort({'createdAt': -1});

    q.exec(function(err, people){
        if (err) {console.log(err); res.sendStatus(500); return;}
        res.json({data: people});
    });
}