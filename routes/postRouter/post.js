var models = require('../../models');
var postData = require('../../models/post/data');

module.exports = function post(req, res) {

    var post = new models.Post();

    for (var prop in postData) {
        if (postData.hasOwnProperty(prop)) {
            if (req.body[prop]) {
                post[prop] = req.body[prop];
            }
            else if (postData[prop].default !== undefined) {
                post[prop] = postData[prop].default;
            }
            else {
                res.status(400).send({error: "some required parameters were not provided or don't respect the required format"});
                res.end();
                return;
            }
        }
    }

    post.save(function (err) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
            return;
        }
        res.json(post);
    });
}