var models = require('../../models');

module.exports = function getOne (req, res) {

    var q = models.Post.findById(req.params.post_id);

    q.exec(function(err, post){
        if (err) { console.log(err); return res.sendStatus(500); }
        if (!post) { return res.status(404).json({error: 'cannot find collection with id: '+req.params.post_id}) }

        res.json({data: post});
    })

}