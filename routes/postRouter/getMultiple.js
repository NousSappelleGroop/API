var models = require('../../models');

module.exports = function getMultiple(req, res) {

    var q = models.Post.find().sort({'createdAt': -1});

    q.exec(function(err, post){
        if (err) {console.log(err); res.sendStatus(500); return;}
        res.json({data: post});
    });
}