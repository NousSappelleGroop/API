var express = require('express');

var router = express.Router();

router.route('/')
    .get(function (req, res) {
        require('./getMultiple')(req, res);
    })
    .post(function (req, res) {
        require('./post')(req, res);
    });

router.route('/:accident_id')
    .get(function (req, res) {
        require('./getOne')(req, res);
    });

module.exports = router;