var models = require('../../models');
var accidentData = require('../../models/accident/data');

module.exports = function post(req, res) {

    var accident = new models.Accident();

    for (var prop in accidentData) {
        if (accidentData.hasOwnProperty(prop)) {
            if (req.body[prop]) {
                accident[prop] = req.body[prop];
            }
            else if (accidentData[prop].default !== undefined) {
                accident[prop] = accidentData[prop].default;
            }
            else {
                res.status(400).send({error: "some required parameters were not provided or don't respect the required format"});
                res.end();
                return;
            }
        }
    }

    accident.save(function (err) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
            return;
        }
        res.json(accident);
    });
}