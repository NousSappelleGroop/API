var models = require('../../models');

module.exports = function getOne (req, res) {

	var q = models.Accident.findById(req.params.accident_id);

	q.exec(function(err, accident){
        if (err) { console.log(err); return res.sendStatus(500); }
        if (!accident) { return res.status(404).json({error: 'cannot find collection with id: '+req.params.accident_id}) }

        res.json(accident);
    })

}