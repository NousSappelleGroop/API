module.exports = function (app) {

    var express = require('express');
    var path = require('path');
    var accidentRouter = require('./accidentRouter');
    var postRouter = require('./postRouter');
    var peopleRouter = require('./peopleRouter');

    app.use('/', express.static(path.resolve(__dirname, '../Front/dist')));

    app.use('/api/accidents', accidentRouter);
    app.use('/api/posts', postRouter);
    app.use('/api/people', peopleRouter);

    app.get('/*', function (req, res) {
        res.sendFile(path.resolve(__dirname, '../Front/dist/index.html'));
    });
}
